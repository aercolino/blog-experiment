# Blog Experiment

This was a programming task that required to build a blog in Ruby on Rails. 

1. CRUD of blog posts
2. upload images
3. search
4. testing


Now it's an experimental blog, based on Angular 2 and Rails 5. It's the result of less than four days of work with no previous  knowledge of Angular 2 (but stale knowledge of Angular 1) and no previous knowledge of Rails 5 (but fresh knowledge of Rails 4).

With respect to the requirements above, I implemented R and U, with a pinch of testing, into a backend Rails 5 API application. For more fun at programming and learning, instead I implemented the frontend using an Angular 2 app. Additionally, given that no blog is a real blog without restricted access to editing, **I added token based authentication**.


### Code

* User Creation:

    * `.../blog-experiment/rails-api $ rails console`
    * `> User.create(username: 'andrea', password: '...').reset_token`

* Backend Server:

    * `.../blog-experiment/rails-api $ rails server`
    * http://localhost:3000/articles.json

* Frontend Server:

    * `.../blog-experiment/angular-app $ ng serve`
    * http://localhost:4200/articles

* Tests

    * `.../blog-experiment/rails-api $ rails test`


## Install development software

I used a brand new MacBook Pro.

```
1. Bash goodies, like prompt and much more
    1. https://github.com/mathiasbynens/dotfiles
2. Install Xcode
3. Install RVM
    1. $ brew install gnupg gnupg2
    2. $ gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
    3. $ \curl -sSL https://get.rvm.io | bash -s stable
    4. $ source ~/.bash_profile
    5. $ rvm autolibs enable (“rvm autolibs homebrew” didn’t work for Ruby later 
4. Config Git
    1. $ git config --global user.name "Andrea Ercolino"
    2. $ git config --global user.email cappuccino.e.cornetto@gmail.com
5. Install Ruby
    1. $ rvm install ruby-2.4.0
6. Update RubyGems
    1. $ gem update --system
7. Update global gems
    1. $ rvm gemset use global
    2. $ gem update
8. Never install docs
    1. $ echo "gem: --no-document" >> ~/.gemrc
9. Install Bundler
    1. $ gem install bundler
10. Install Nokogiri
    1. $ gem install nokogiri
11. Install Rails
    1. $ rvm use ruby-2.4.0@rails5.0 --create
    2. $ gem install rails
12. Uninstall OpenSSL 2.0.3 in the global gemset because of a bug in Bundler 1.14.3 fixed by https://github.com/bundler/bundler/pull/5388
13. Install node
    1. $ sudo chown -R $(whoami) /usr/local
    2. $ brew install node
    3. $ node -v
        1. v7.5.0
    4. $ npm -v
        1. 4.1.2
    5. Errors: https://github.com/npm/npm/issues/15708#issuecomment-278840455
14. Install Angular CLI
    1. $ npm install -g @angular/cli
        1. There will be some unmet peer dependencies (impossible to get rid of completely).
        2. Install them recursively just once.
        3. Ignore additional unmet peer dependencies of those manually installed because the feature is broken.
15. Install PostgreSQL
    1. $ brew install postgres
    2. $ brew services start postgresql
```

## Program the blog

```
1. Create ~/dev/ruby/blog-experiment/angular-app
    1. $ ng new angular-app
2. Create ~/dev/ruby/blog-experiment/rails-api
    1. $ mkdir rails-api
    2. $ cd rails-api
    3. $ rvm use ruby-2.4.0@rails-api --ruby-version --create
    4. $ gem install rails
    5. $ rails new . --api -d postgresql
    6. $ rails db:create
    7. $ rails generate scaffold article headline:string byline:string lead:string body:text
    8. $ rails db:migrate
3. Create frontend for CRUD
4. Add authentication
5. TODO: Add Image Uploading
    1. https://www.pluralsight.com/guides/ruby-ruby-on-rails/handling-file-upload-using-ruby-on-rails-5-api
6. TODO: Add Search
    1. https://github.com/Casecommons/pg_search
```

### Essential References

* http://railsapps.github.io/installrubyonrails-mac.html
* https://www.angularonrails.com/angular-2-tour-heroes-tutorial-rails-backend/
* https://angular.io/docs/ts/latest/tutorial/
* https://www.jetbrains.com/help/ruby/2016.3/transpiling-typescript-to-javascript.html
* https://labs.kollegorna.se/blog/2015/04/build-an-api-now/
