import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { Router }            from '@angular/router';

import 'rxjs/add/operator/switchMap';

import { Article } from './article';
import { ArticleService } from './article.service';

@Component({
  moduleId: module.id,
  selector: 'article-read',
  templateUrl: './article-read.component.html',
  styleUrls: ['./app.component.css']
})
export class ArticleReadComponent implements OnInit {
  article: Article;
  loggedIn: boolean;

  constructor(
    private articleService: ArticleService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router
  ) {
    this.loggedIn = !!localStorage.getItem('auth_token');
  }

  ngOnInit(): void {
    this.route.params
      .switchMap((params: Params) => this.articleService.requestRead(+params['id']))
      .subscribe(article => this.article = article);
  }

  goBack(): void {
    this.location.back();
  }

  edit(): void {
    this.router.navigate(['/article/edit/', this.article.id]);
  }
}
