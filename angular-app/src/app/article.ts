
export class Article {
  id: number;
  headline: string;
  byline: string;
  body: string;
  created_at: string;
  updated_at: string;
}
