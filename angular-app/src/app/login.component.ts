import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { SessionService } from './session.service';

@Component({
  moduleId: module.id,
  selector: 'login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;

  constructor(
    private sessionService: SessionService,
    private router: Router
  ) {}

  ngOnInit(): void {
    if (localStorage.getItem('auth_token')) {
      this.sessionService.requestDestroy()
        .catch(() => {});
    }
  }

  login(): void {
    this.sessionService.requestCreate(this.username, this.password)
      .then(() => this.router.navigate(['/']));
  }
}
