import { Injectable } from '@angular/core';
import { Article } from './article';
import { Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { SessionService } from './session.service';

@Injectable()
export class ArticleService {
  private resourcesUrl = 'http://localhost:3000/articles';

  constructor(
    private http: Http,
    private sessionService: SessionService
  ) {}

  requestIndex(): Promise<Article[]> {
    return this.http.get(this.resourcesUrl)
      .toPromise()
      .then(response => response.json() as Article[])
      .catch(this.handleError);
  }

  requestRead(id: number): Promise<Article> {
    const url = `${this.resourcesUrl}/${id}`;
    return this.http
      .get(url)
      .toPromise()
      .then(response => response.json() as Article)
      .catch(this.handleError);
  }

  requestUpdate(article: Article): Promise<Article> {
    const url = `${this.resourcesUrl}/${article.id}`;
    return this.http
      .put(url, JSON.stringify(article), this.sessionService.auth_options())
      .toPromise()
      .then(() => article)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

}
