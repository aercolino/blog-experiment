import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ArticleListComponent } from './article-list.component';
import { ArticleReadComponent } from './article-read.component';
import { ArticleEditComponent } from './article-edit.component';
import { ArticleService } from './article.service';
import { SessionService } from "./session.service";
import { LoggedIn } from "./logged-in.service";

import { AppRoutingModule } from './app-routing.module';
import {LoginComponent} from "./login.component";

@NgModule({
  declarations: [
    AppComponent,
    ArticleListComponent,
    ArticleReadComponent,
    ArticleEditComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [
    ArticleService,
    SessionService,
    LoggedIn
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
