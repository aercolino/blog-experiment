import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import 'rxjs/add/operator/switchMap';

import { Article } from './article';
import { ArticleService } from './article.service';

@Component({
  moduleId: module.id,
  selector: 'article-edit',
  templateUrl: './article-edit.component.html',
  styleUrls: ['./app.component.css']
})
export class ArticleEditComponent implements OnInit {
  article: Article;

  constructor(
    private articleService: ArticleService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.route.params
      .switchMap((params: Params) => this.articleService.requestRead(+params['id']))
      .subscribe(article => this.article = article);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.articleService.requestUpdate(this.article)
      .then(() => this.goBack());
  }
}
