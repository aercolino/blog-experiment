import { Injectable } from '@angular/core';
import { Headers, RequestOptions, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class SessionService {
  private resourcesUrl = 'http://localhost:3000/session';

  constructor(
    private http: Http
  ) {}

  requestCreate(username, password): Promise<void> {
    let options = new RequestOptions({ headers: new Headers({
      'Content-Type': 'application/json'
    }) });
    return this.http
      .post(this.resourcesUrl, {username, password}, options)
      .toPromise()
      .then(response => localStorage.setItem('auth_token', response.json()))
      .catch(this.handleError);
  }

  requestDestroy(): Promise<void> {
    return this.http
      .delete(this.resourcesUrl, this.auth_options())
      .toPromise()
      .then(() => localStorage.removeItem('auth_token'))
      .catch(this.handleError);
  }

  auth_options(): RequestOptions {
    return new RequestOptions({ headers: new Headers({
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('auth_token')
    }) });
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

}
