import { Component, OnInit } from '@angular/core';
import { Router }            from '@angular/router';

import { Article } from './article';
import { ArticleService } from './article.service';

@Component({
  moduleId: module.id,
  selector: 'articles',
  templateUrl: './article-list.component.html',
  styleUrls: ['article-list.component.css']
})
export class ArticleListComponent implements OnInit {
  articles: Article[];

  constructor(
    private articleService: ArticleService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.requestIndex();
  }

  requestIndex(): void {
    this.articleService.requestIndex()
      .then(articles => this.articles = articles);
  }

  read(article): void {
    this.router.navigate(['/article/read/', article.id]);
  }
}
