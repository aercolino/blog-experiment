import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoggedIn } from './logged-in.service';
import { ArticleListComponent } from './article-list.component';
import { ArticleReadComponent } from './article-read.component';
import { ArticleEditComponent } from './article-edit.component';
import { LoginComponent } from './login.component';

const routes: Routes = [
  { path: '', redirectTo: '/articles', pathMatch: 'full' },
  { path: 'article/read/:id',  component: ArticleReadComponent },
  { path: 'article/edit/:id',  component: ArticleEditComponent, canActivate: [LoggedIn] },
  { path: 'articles', component: ArticleListComponent },
  { path: 'login', component: LoginComponent }
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
