class ApplicationController < ActionController::API

  include ActionController::HttpAuthentication::Token
  include ActionController::HttpAuthentication::Token::ControllerMethods

  attr_reader :current_user

  private

  def api_error(status: 500, errors: [])
    unless Rails.env.production?
      puts errors.full_messages if errors.respond_to? :full_messages
    end

    { json: errors.to_json, status: status }
  end

  def api_token(user)
    { json: encode_credentials(user.auth_token, { username: user.username }).to_json }
  end

  def set_current_user
    @current_user = authenticate_or_request_with_http_token do |token, options|
      options.present? or break
      options[:username].present? or break
      user = User.find_by(username: options[:username]) or break
      user.token_recently_checked? or break
      user.valid_token?(token) or break
      user.update!(checked_at: Time.now) unless user.token_just_checked?
      user
    end
    render api_error(status: :not_found, errors: ['Invalid user.']) unless current_user
  end

end
