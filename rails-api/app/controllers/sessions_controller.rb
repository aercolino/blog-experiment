class SessionsController < ApplicationController

  before_action :set_current_user, except: :create

  def create
    current_user = User.find_by(username: create_params[:username])
                       .try(:authenticate, create_params[:password])
    render api_error(status: :not_found) and return unless current_user

    current_user.reset_token!
    render api_token(current_user)
  end

  def destroy
    current_user.reset_token!
    render status: :ok
  end


  private

  def create_params
    params.permit(:username, :password)
  end

end
