class User < ApplicationRecord

  has_secure_password

  FRESH_INTERVAL = 30.minutes
  FRESHEST_INTERVAL = 3.minutes

  def reset_token!
    self.checked_at = Time.now
    self.auth_token = SecureRandom.base64(64)
    save!
  end

  def valid_token?(token)
    a = ::Digest::SHA256.hexdigest(self.auth_token)
    b = ::Digest::SHA256.hexdigest(token)
    ActiveSupport::SecurityUtils.secure_compare(a, b)
  end

  def token_recently_checked?
    self.checked_at.present? or return false
    Time.now - self.checked_at < FRESH_INTERVAL
  end

  def token_just_checked?
    self.checked_at.present? or return false
    Time.now - self.checked_at < FRESHEST_INTERVAL
  end

end
