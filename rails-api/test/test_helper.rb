ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  def login(username)
    user = User.find_by(username: username)
    user.reset_token!
    ActionController::HttpAuthentication::Token.encode_credentials(user.auth_token, { username: user.username })
  end
end
