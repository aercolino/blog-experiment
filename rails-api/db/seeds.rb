# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Article.create([
                   {
                       headline: 'Welcome',
                       byline:   'in collaboration with Angular and Rails',
                       lead:     'Will Andrea work remotely or not?',
                       body:     'Everything looks good, so... YES!'
                   },
                   {
                       headline: 'How do you do?',
                       byline:   nil,
                       lead:     'Andrea wants to be a nomad.',
                       body:     'Living in one country and working for another, cool :)'
                   },
               ])
