class CreateArticles < ActiveRecord::Migration[5.0]
  def change
    create_table :articles do |t|
      t.string :headline
      t.string :byline
      t.string :lead
      t.text :body

      t.timestamps
    end
  end
end
