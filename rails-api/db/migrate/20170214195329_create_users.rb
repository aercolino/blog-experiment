class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :username
      t.string :password
      t.string :auth_token
      t.datetime :checked_at

      t.timestamps
    end
  end
end
